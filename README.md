Gladness
========

Gladness is a collection of Gitlab CI pipelines, configuration files,
container images and utilities to help build, test and develop mostly
(but not exclusively) projects under vornet group on gitlab.com.
